#!/bin/bash
FILE=./.env
if [ -f "$FILE" ]; then
    echo "removing existing .env file"
    rm $FILE 
fi
echo "Adding Env Variables to .env file"
echo CLIENT_ID_SPOTIFY=2dcfb393a5354aaab2e2eb07f00c4d0b >> .env
echo CLIENT_SECRET_SPOTIFY=4ff863850c0a4f15b1fcb9b10c70c907 >> .env
echo REDIRECT_URI= http://test-svr.tk:8888/callback/ >> .env
echo REACT_DEV_APP_URL=http://test-svr.tk:4000 >> .env
echo REACT_PROD_APP_URL=http://test-svr.tk:8000 >> .env
echo SERVER_URL=http://test-svr.tk:8888 >> .env
echo MODE=production >> .env


